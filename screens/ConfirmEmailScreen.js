import { View, Text, Image, StyleSheet, ScrollView } from "react-native";
import React, { useState } from "react";
import Logo from "../../../assets/images/nomify-logo.png";
import CustomInput from "../components/CustomInput/CustomInput";
import CustomButton from "../components/CustomButton/CustomButton";
import SocialSignInButton from "../components/SocialSignInButton/SocialSignInButton";
import { useNavigation } from "@react-navigation/native";

const ConfirmEmailScreen = () => {
  const navigation = useNavigation();

  const [code, setCode] = useState("");

  const onSendPressed = () => {
    console.warn("Register");
  };

  const onTermOfUse = () => {
    console.warn("Terms of Use");
  };

  const onPrivacyPolicy = () => {
    console.warn("Privacy Policy");
  };

  const onResendPress = () => {
    console.warn("Resend Code");
  };

  const onResendPressed = () => {
    console.warn("Sign In");
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.root}>
        <Image source={Logo} />
        <Text style={styles.title}>Confirm your Email</Text>
        <CustomInput
          placeholder="Enter your confirmation code"
          value={code}
          setValue={setCode}
        />

        <CustomButton
          text="Confirm"
          onPress={() => navigation.navigate("Home")}
        />

        <CustomButton
          text="Resend Code"
          onPress={onResendPress}
          type="FOURTH"
        />
        <CustomButton
          text="Back to Sign In"
          onPress={() => navigation.navigate("SignIn")}
          type="TRETIARY"
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#051C60",
    margin: 30,
  },
  terms: {
    color: "gray",
    marginVertical: 10,
  },
  link: {
    color: "#98e365",
  },
});

export default ConfirmEmailScreen;
