const underweightData = {
  breakfast: [
    {
      image: require("../../../assets/images/Longsilog.jpg"),
      name: "Longsilog with Fruit",
      ingredients: [
        "Longganisa sausages (available in Filipino or Asian grocery stores)",
        "4 cups cooked white rice (preferably day-old)",
        "5-6 cloves garlic, minced",
        "2 tablespoons vegetable oil",
        "Salt to taste",
        "Slices of mango, pineapple, or any preferred fruit",
      ],
    },
    {
      image: require("../../../assets/images/Scrambled.jpg"),
      name: "Scrambled Eggs with Tomatoes, Onions, and Cheese",
      ingredients: [
        "2 eggs",
        "½ cup chopped tomatoes",
        "¼ cup chopped onions",
        "½ cup shredded cheese (cheddar or mozzarella)",
        "1 tablespoon olive oil or cooking spray",
        "Salt and pepper to taste",
      ],
    },
    {
      image: require("../../../assets/images/Sopas.jpg"),
      name: "Chicken Sopas with Vegetables",
      ingredients: [
        "1 boneless, skinless chicken breast, shredded",
        "2 cups assorted vegetables (carrots, sayote, chayote, kangkong), chopped",
        "1 cup chopped cabbage",
        "¼ cup chopped onion",
        "2 cloves garlic, minced",
        "½ cup egg noodles",
        "4 cups chicken broth",
        "1 tablespoon olive oil or cooking spray",
        "Salt and pepper to taste",
      ],
    },
    {
      image: require("../../../assets/images/Tapsilog.jpg"),
      name: "Tapsilog (Tapa, Fried Rice, Fried Egg) with Apple",
      ingredients: [
        "1 lb beef sirloin or tenderloin, thinly sliced",
        "1/4 cup soy sauce",
        "1/4 cup calamansi juice (or lemon juice)",
        "8 cloves garlic, minced (total for both tapa and fried rice)",
        "2 tablespoons brown sugar",
        "1/2 teaspoon ground black pepper",
        "5 tablespoons vegetable oil (total for tapa, fried rice, and eggs)",
        "4 cups cooked white rice (preferably day-old)",
        "Salt to taste",
        "2 apples, sliced (for serving)",
      ],
    },
    {
      // image: require("../../../assets/images/Champorado.jpg"),
      name: "Champorado with Tuyo and Banana",
      ingredients: [
        "1 cup glutinous rice",
        "4 cups water",
        "1/2 cup cocoa powder or 4-5 pieces tablea (Filipino chocolate)",
        "1/2 cup sugar (adjust to taste)",
        "Condensed milk or evaporated milk (for serving)",
        "Dried salted fish (tuyo)",
        "Vegetable oil (for frying)",
        "2-3 ripe bananas (saba bananas are preferred, but any type will do)",
      ],
    },
    {
      // image: require("../../../assets/images/Pancakes.jpg"),
      name: "Pancakes with Sweet Potato and Chia Seeds",
      ingredients: [
        "½ cup mashed sweet potato",
        "½ cup all-purpose flour or whole wheat flour",
        "1 egg",
        "½ cup milk",
        "1 tablespoon chia seeds",
        "1 teaspoon baking powder",
        "¼ teaspoon cinnamon powder (optional)",
        "1 tablespoon cooking oil or butter",
        "Honey or maple syrup (to taste)",
      ],
    },
    {
      image: require("../../../assets/images/Bangsilog.jpg"),
      name: "Healthy Bangsilog",
      ingredients: [
        "1 serving lean bangus (milkfish)",
        "½ cup cooked brown rice",
        "2 eggs (scrambled)",
        "½ cup chopped tomatoes (optional)",
        "¼ cup chopped onions (optional)",
        "1 tablespoon olive oil or cooking spray",
        "Salt and pepper to taste",
      ],
    },
  ],

  lunch: [
    {
      image: require("../../../assets/images/Afritada.jpg"),
      name: "Chicken Afritada",
      ingredients: [
        "1/4 kg chicken breast, cut into serving pieces (skinless, boneless)",
        "1 tbsp olive oil (or less)",
        "1 medium onion, chopped",
        "3 cloves garlic, minced",
        "1 medium tomato, chopped",
        "1 medium potato, diced",
        "1 medium carrot, diced",
        "1/2 red bell pepper, sliced",
        "1/2 green bell pepper, sliced",
        "1/4 cup green peas (optional)",
        "1 cup tomato sauce (or fresh tomato puree)",
        "1 cup water or chicken broth",
        "1 bay leaf",
        "Salt and pepper to taste",
      ],
    },
    {
      image: require("../../../assets/images/Adobo Flakes.jpg"),
      name: "Chicken Adobo Flakes with Brown Rice and Sauteed Mixed Vegetables",
      ingredients: [
        "1 boneless, skinless chicken breast or thighs, cut into pieces",
        "¼ cup soy sauce",
        "¼ cup vinegar",
        "2 cloves garlic, crushed",
        "½ teaspoon black peppercorns",
        "2 bay leaves",
        "½ cup cooked brown rice",
        "2 cups assorted vegetables (broccoli, carrots, cauliflower), chopped",
        "1 tablespoon olive oil or cooking spray",
      ],
    },
    {
      image: require("../../../assets/images/Canton.jpg"),
      name: "Pancit Canton with Vegetables and Sliced Oranges",
      ingredients: [
        "1/2 cup dried egg noodles (pancit canton)",
        "1 tablespoon vegetable oil",
        "1/2 onion, chopped",
        "2 cloves garlic, minced",
        "1 cup assorted vegetables (broccoli florets, carrots, bell peppers, etc.), chopped",
        "1/2 lb protein (chicken, pork, shrimp, or tofu), sliced and cooked (optional)",
        "1/4 cup water or chicken broth",
        "1/4 cup soy sauce (low sodium)",
        "1 tablespoon oyster sauce (optional)",
        "1 teaspoon cornstarch (mixed with 1 tablespoon water to make a slurry)",
        "Salt and pepper to taste",
        "2 pieces of oranges",
      ],
    },
    {
      image: require("../../../assets/images/Pochero.jpg"),
      name: "Pork Pochero with Vegetables",
      ingredients: [
        "1 lb pork belly, cut into 2-inch cubes (bone-in or boneless, depending on preference)",
        "1/2 cup green peas (fresh or frozen)",
        "1/2 cup chickpeas (garbanzos), soaked overnight or for at least 4 hours",
        "1/2 head of cabbage, quartered",
        "1 ripe saba banana (plantain), peeled and cut into chunks",
        "1 large potato, peeled and quartered",
        "1 large carrot, peeled and cut into chunks",
        "1 onion, chopped",
        "4 cloves garlic, minced",
        "1 tablespoon tomato paste",
        "1 teaspoon whole black peppercorns",
        "1 bay leaf",
        "1 tablespoon fish sauce (adjust to taste)",
        "Salt to taste",
        "4 cups water",
      ],
    },
    {
      image: require("../../../assets/images/Tapa.jpg"),
      name: "Beef Tapa with Brown Rice and Sauted Spinach",
      ingredients: [
        "3 oz lean beef tapa, sliced",
        "½ cup cooked brown rice",
        "2 cups fresh spinach",
        "1 tablespoon olive oil or cooking spray",
        "1 clove garlic, minced (optional)",
        "1 tablespoon soy sauce (optional)",
        "Lemon or lime juice (optional, to taste)",
      ],
    },
    {
      image: require("../../../assets/images/Laing.jpg"),
      name: "Chicken Ginataang Laing with Munggo Beans and Vegetables",
      ingredients: [
        "1 boneless, skinless chicken breast or thigh, cut into pieces",
        "1 cup coconut milk",
        "2 cups taro leaves (gabi leaves), pre-washed",
        "1 tablespoon shrimp paste (bagoong alamang)",
        "½ cup cooked munggo beans",
        "1 cup assorted vegetables (okra, eggplant), sliced",
        "1 tablespoon olive oil or cooking spray",
      ],
    },
    {
      image: require("../../../assets/images/Shanghai.jpg"),
      name: "Filipino Shanghai with Brown Rice and Sweet and Sour Sauce",
      ingredients: [
        "1 cup assorted vegetables (julienned (optional)",
        "Lumpia wrappers (quantity depends on desired serving size)",
        "½ cup cooked brown rice",
        "For the sweet and sour sauce:",
        "2 tablespoons sugar",
        "2 tablespoons vinegar",
        "¼ cup water",
        "1 tablespoon ketchup (optional)",
        "¼ cup carrots, julienned (optional)",
        "¼ cup onions, julienned (optional)",
      ],
    },
  ],

  dinner: [
    {
      image: require("../../../assets/images/Inasal.jpg"),
      name: "Chicken Inasal with Ensaladang Mango",
      ingredients: [
        "2 lbs chicken thighs, bone-in (skinless or with skin, depending on your preference)",
        "1/2 cup annatto oil (atsuete oil) - you can substitute with paprika for a milder flavor",
        "1/4 cup calamansi juice (or substitute with lime juice)",
        "1/4 cup soy sauce (low sodium recommended)",
        "2 tablespoons minced garlic",
        "2 tablespoons chopped ginger",
        "1 tablespoon brown sugar",
        "1 teaspoon ground black pepper",
        "1/2 teaspoon salt (or to taste)",
        "1/4 cup water (optional)",
      ],
    },
    {
      image: require("../../../assets/images/Tinola.jpg"),
      name: "Chicken Tinola with Papaya, Malunggay Leaves, and Brown Rice",
      ingredients: [
        "1 boneless, skinless chicken breast or thigh, cut into pieces",
        "1 cup unripe papaya, chopped",
        "½ cup malunggay leaves (moringa leaves)",
        "1 inch ginger, sliced",
        "2 cloves garlic, minced",
        "1 tablespoon fish sauce (patis)",
        "½ teaspoon black peppercorns",
        "½ cup cooked brown rice",
      ],
    },
    {
      image: require("../../../assets/images/PorkSinigang.jpg"),
      name: "Pork Sinigang sa Miso with Tofu and Vegetables",
      ingredients: [
        "4 oz pork belly or shoulder, cut into pieces",
        "2 tablespoons miso paste",
        "½ cup tamarind juice or guava juice",
        "½ cup chopped onions",
        "½ cup chopped tomatoes",
        "½ cup radish, sliced",
        "½ cup okra, whole",
        "½ cup green beans, trimmed",
        "½ cup firm tofu, cubed",
        "½ cup cooked brown rice",
      ],
    },
    {
      image: require("../../../assets/images/Ginataang Shrimp.jpg"),
      name: "Ginataang Hipon (Shrimp) with Kangkong, Squash, and Brown Rice",
      ingredients: [
        "1 cup peeled and deveined shrimp",
        "1 cup coconut milk",
        "2 cups kangkong (water spinach), trimmed",
        "1 cup squash (kabocha or calabaza), cut into chunks",
        "1 tablespoon fish sauce (patis)",
        "1 clove garlic, minced",
        "1 inch ginger, sliced",
        "½ cup cooked brown rice",
      ],
    },
    {
      image: require("../../../assets/images/Beef Kare-kare.jpg"),
      name: "Beef Kare-Kare with Vegetables",
      ingredients: [
        "4 oz beef chuck or oxtail, cut into pieces",
        "2 tablespoons peanut butter",
        "½ cup banana blossom (puso ng saging), sliced (optional)",
        "1 tablespoon shrimp paste (bagoong alamang)",
        "½ cup Baguio beans, cut into pieces",
        "½ cup string beans, trimmed",
        "1 whole eggplant",
        "1 cup bok choy or pechay, stalks separated",
        "1 tablespoon fish sauce (patis)",
        "½ cup peeled and deveined shrimp (optional)",
        "½ cup cooked brown rice",
      ],
    },
    {
      // image: require("../../../assets/images/Tofu Vegetables.jpg"),
      name: "Stir-fried Tofu with Mixed Vegetables and Brown Rice",
      ingredients: [
        "½ cup firm tofu, cubed",
        "2 cups assorted vegetables (broccoli, carrots, bell peppers), sliced",
        "Eggs (optional, beaten for scrambled tofu)",
        "2 tablespoons soy sauce",
        "1 tablespoon oyster sauce (optional)",
        "1 clove garlic, minced",
        "1 inch ginger, minced",
        "½ cup cooked brown rice",
      ],
    },
    {
      image: require("../../../assets/images/Chicken Curry.jpg"),
      name: "Chicken Curry with Brown Rice",
      ingredients: [
        "1 boneless, skinless chicken breast or thigh, cut into pieces",
        "2 tablespoons curry powder",
        "1 cup coconut milk",
        "1 tablespoon fish sauce (patis)",
        "½ cup chopped onions",
        "1 clove garlic, minced",
        "1 inch ginger, minced",
        "½ cup potatoes, cubed (optional)",
        "½ cup carrots, chopped (optional)",
        "½ cup bell peppers, sliced (optional)",
        "½ cup cooked brown rice",
      ],
    },
  ],
};

export default underweightData;
