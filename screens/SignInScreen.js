import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  useWindowDimensions,
  ScrollView,
  Alert,
} from "react-native";
import Logo from "../../../assets/images/nomify-logo.png";
import icon from "../../../assets/images/logo.jpg";
import React, { useState } from "react";
import { useNavigation } from "@react-navigation/native";
import CustomInput from "../components/CustomInput";
import CustomButton from "../components/CustomButton/CustomButton";
import { Ionicons } from "@expo/vector-icons";
import auth from "../../../services/firebaseAuth";
import { signInWithEmailAndPassword } from "firebase/auth";

export default function SignInScreen() {
  const navigation = useNavigation();

  const [email, setEmail] = useState("");

  const [password, setPassword] = useState("");

  const { height } = useWindowDimensions();

  const [passwordVisible, setPasswordVisible] = useState(false);

  const onSignInPressed = () => {
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        console.log(user);
        // Navigate to the home screen after successful sign-in
        navigation.navigate("Home");
      })
      .catch((error) => {
        console.error(error);
        Alert.alert(
          "Sign In Error",
          "No account found. Please check your credentials or sign up."
        );
      });
  };

  const onForgotPressed = () => {
    navigation.navigate("Forgot");
  };

  const onSignUpPressed = () => {
    navigation.navigate("SignUp");
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.root}>
        <Image source={Logo} />
        <Image source={icon} style={[styles.logo, { height: height * 0.2 }]} />
        <CustomInput placeholder="Email" value={email} setValue={setEmail} />
        <CustomInput
          placeholder="Password"
          value={password}
          setValue={setPassword}
          secureTextEntry={!passwordVisible}
        />
        <TouchableOpacity
          onPress={() => setPasswordVisible(!passwordVisible)}
          style={styles.passicon}
        >
          <Ionicons name={passwordVisible ? "eye" : "eye-off"} size={24} />
        </TouchableOpacity>
        <CustomButton text="Sign In" onPress={onSignInPressed} />
        <CustomButton
          text="Forgot Password?"
          onPress={onForgotPressed}
          type="TRETIARY"
        />
        <CustomButton
          text="Don't have an account? Create Now"
          onPress={onSignUpPressed}
          type="TRETIARY"
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
  },
  logo: {
    width: "43%",
    maxWidth: 300,
    maxheight: 200,
    borderRadius: 20,
    marginTop: 40,
    marginBottom: 50,
  },
  passicon: {
    position: "relative",
    bottom: 43,
    left: 150,
  },
});
