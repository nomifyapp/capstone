/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, Button, Alert, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const MedicalInfo = ({ navigation }) => {
  const [allergies, setAllergies] = useState('');

  useEffect(() => {
    const loadAllergies = async () => {
      try {
        const savedAllergies = await AsyncStorage.getItem('allergies');
        if (savedAllergies) {
          setAllergies(savedAllergies);
        }
      } catch (error) {
        console.error('Failed to load allergies', error);
      }
    };

    loadAllergies();
  }, []);

  const showAlert = () => {
    Alert.alert(
      'Allergies Saved',
      'Your allergies information has been saved successfully.',
      [
        {
          text: 'OK',
          onPress: () => console.log('OK Pressed'),
        },
      ]
    );
  };

  const saveAllergies = async () => {
    try {
      await AsyncStorage.setItem('allergies', allergies);
      showAlert();
    } catch (error) {
      console.error('Failed to save allergies', error);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Medical Information</Text>
      <View style={styles.card}>
        <Text style={styles.label}>Allergies:</Text>
        <TextInput
          style={styles.input}
          value={allergies}
          onChangeText={setAllergies}
          placeholder="Enter your allergies"
        />
      </View>
      <Button title="Save" onPress={saveAllergies} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    backgroundColor: '#f5f5f5',
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  card: {
    width: '90%',
    padding: 20,
    marginVertical: 10,
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 3,
    shadowColor: '#000',
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { width: 0, height: 2 },
  },
  label: {
    fontSize: 18,
    marginBottom: 10,
  },
  input: {
    width: '100%',
    padding: 10,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 5,
  },
});

export default MedicalInfo;
