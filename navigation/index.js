import { View, Text } from "react-native";
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import SignInScreen from "../screens/SignInScreen";
import HomeScreen from "../screens/HomeScreen";
import Ingredients from "../screens/Ingredients";
import ForgotPassword from "../screens/ForgotPassword";
import SignUpScreen from "../screens/SignUpScreen";
import NewPasswordScreen from "../screens/NewPasswordScreen";
import ConfirmEmailScreen from "../screens/ConfirmEmailScreen";
import Underweight from "../screens/Underweight";
import UnderweightIngredients from "../screens/UnderweightIngredients";
import Normal from "../screens/Normal";
import NormalIngredients from "../screens/NormalIngredients";
import Overweight from "../screens/Overweight";
import OverweightIngredients from "../screens/OverweightIngredients";
import Obese from "../screens/Obese";
import ObseseIngredients from "../screens/ObeseIngdredients";
import UserProfile from '../screens/UserProfile';
import MedicalInfo from '../screens/MedicalInfo';
import ContactUsScreen from '../screens/ContactUsScreen';
import MetricsScreen from '../screens/MetricsScreen';


import HistoryScreen from "../screens/HistoryScreen";


const Stack = createNativeStackNavigator();

export default function AppNavigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="SignIn"
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="SignIn" component={SignInScreen} />
        <Stack.Screen name="Forgot" component={ForgotPassword} />
        <Stack.Screen name="SignUp" component={SignUpScreen} />
        <Stack.Screen name="NewPass" component={NewPasswordScreen} />
        <Stack.Screen name="Confirm" component={ConfirmEmailScreen} />
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Ingredients" component={Ingredients} />
        <Stack.Screen name="Underweight" component={Underweight} />
        <Stack.Screen
          name="UnderweightIngredients"
          component={UnderweightIngredients}
        />
        <Stack.Screen name="Normal" component={Normal} />
        <Stack.Screen name="NormalIngredients" component={NormalIngredients} />
        <Stack.Screen name="Overweight" component={Overweight} />
        <Stack.Screen
          name="OverweightIngredients"
          component={OverweightIngredients}
        />
        <Stack.Screen name="Obese" component={Obese} />
        <Stack.Screen name="ObeseIngredients" component={ObseseIngredients} />
        <Stack.Screen name="Profile" component={UserProfile} />
        <Stack.Screen name="MedicalInfo" component={MedicalInfo} />
        <Stack.Screen name="ContactUs" component={ContactUsScreen} />
        <Stack.Screen name="Metrics" component={MetricsScreen} />

        <Stack.Screen name="History" component={HistoryScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
