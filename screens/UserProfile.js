/* eslint-disable prettier/prettier */
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const UserProfile = ({ route, navigation }) => {
  const { age, weight, height } = route.params;

  return (
    <View style={styles.container}>
      <Text style={styles.title}>User Profile</Text>
      <View style={styles.card}>
        <Text style={styles.info}>Age: {age}</Text>
        <Text style={styles.info}>Weight: {weight} kg</Text>
        <Text style={styles.info}>Height: {height} cm</Text>
      </View>
      <Button title="View Medical Info" onPress={() => navigation.navigate('MedicalInfo', { age, weight, height })} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    backgroundColor: '#f5f5f5',
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  card: {
    width: '90%',
    padding: 20,
    marginVertical: 10,
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 3,
    shadowColor: '#000',
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { width: 0, height: 2 },
  },
  info: {
    fontSize: 18,
    marginBottom: 10,
  },
});

export default UserProfile;
