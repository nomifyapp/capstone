import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
} from "react-native";
import { useRoute, useNavigation } from "@react-navigation/native";
import { Ionicons } from "@expo/vector-icons";
import normalData from "../screens/NormalData";

const NormalIngredients = () => {
  const route = useRoute();
  const navigation = useNavigation();
  const mealName = route.params.meal;
  let normalDataItem;

  // State to track meal completion
  const [mealCompleted, setMealCompleted] = useState(false);

  // Use Object.entries to iterate over the normalData object and find the matching meal
  const [mealType, meals] = Object.entries(normalData).find(([_, meals]) =>
    meals.some((meal) => meal.name.toLowerCase() === mealName.toLowerCase())
  );
  if (mealType) {
    normalDataItem = meals.find(
      (meal) => meal.name.toLowerCase() === mealName.toLowerCase()
    );
  }

  // Function to mark meal as completed
  const markMealCompleted = () => {
    setMealCompleted(true);
    Alert.alert("Meal Completed", "You have successfully completed this meal.");
  };

  // Navigate to the next meal if the current meal is completed
  const navigateToNextMeal = () => {
    if (mealCompleted) {
      navigation.goBack();
    } else {
      Alert.alert("Meal Not Completed", "Please complete this meal first.");
    }
  };

  return (
    <ScrollView style={styles.container}>
      {normalDataItem ? (
        <>
          <View style={styles.mealImageContainer}>
            <Image source={normalDataItem.image} style={styles.mealImage} />
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={styles.backButton}
            >
              <Ionicons name="arrow-back" size={24} color="white" />
            </TouchableOpacity>
          </View>
          <View style={styles.mealInfoContainer}>
            <Text style={styles.mealName}>{normalDataItem.name}</Text>
          </View>
          <View style={styles.ingredientsContainer}>
            <Text style={styles.ingredientsLabel}>Ingredients:</Text>
            {normalDataItem.ingredients.map((ingredient, index) => (
              <View key={index} style={styles.ingredientItem}>
                <Text style={styles.ingredientText}>• {ingredient}</Text>
              </View>
            ))}
          </View>
          <TouchableOpacity
            onPress={markMealCompleted}
            style={styles.completeButton}
          >
            <Text style={styles.completeButtonText}>Mark as Completed</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={navigateToNextMeal}
            style={styles.nextButton}
          >
            <Text style={styles.nextButtonText}>Next Meal</Text>
          </TouchableOpacity>
        </>
      ) : (
        <Text>No meal found</Text>
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mealImageContainer: {
    position: "relative",
  },
  mealImage: {
    width: "100%",
    height: 300,
    backgroundColor: "#ccc",
  },
  backButton: {
    position: "absolute",
    top: 30,
    left: 10,
    backgroundColor: "black",
    padding: 10,
    borderRadius: 25,
    marginTop: 20,
  },
  mealInfoContainer: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    marginTop: -20,
  },
  mealName: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
    marginTop: 20,
  },
  ingredientsContainer: {
    backgroundColor: "white",
    borderRadius: 10,
    padding: 20,
    height: 500,
  },
  ingredientsLabel: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
  },
  ingredientItem: {
    marginBottom: 10,
    marginLeft: 30,
  },
  ingredientText: {
    fontSize: 15,
  },
  completeButton: {
    backgroundColor: "green",
    padding: 15,
    borderRadius: 10,
    marginVertical: 10,
    alignItems: "center",
  },
  completeButtonText: {
    color: "white",
    fontSize: 18,
    fontWeight: "bold",
  },
  nextButton: {
    backgroundColor: "blue",
    padding: 15,
    borderRadius: 10,
    alignItems: "center",
  },
  nextButtonText: {
    color: "white",
    fontSize: 18,
    fontWeight: "bold",
  },
});

export default NormalIngredients;
