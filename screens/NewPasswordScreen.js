import { View, Text, Image, StyleSheet, ScrollView } from "react-native";
import React, { useState } from "react";
import Logo from "../../../assets/images/nomify-logo.png";
import CustomInput from "../components/CustomInput/CustomInput";
import CustomButton from "../components/CustomButton/CustomButton";
import SocialSignInButton from "../components/SocialSignInButton/SocialSignInButton";
import { useNavigation } from "@react-navigation/native";

const NewPasswordScreen = () => {
  const navigation = useNavigation();

  const [code, setCode] = useState("");

  const [newPassword, setnewPassword] = useState("");

  const onSubmitPressed = () => {
    console.warn("Register");
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.root}>
        <Image source={Logo} />
        <Text style={styles.title}>Reset Password</Text>
        <CustomInput
          placeholder="Enter your code"
          value={code}
          setValue={setCode}
        />

        <CustomInput
          placeholder="Enter your new password"
          value={newPassword}
          setValue={setnewPassword}
        />

        <CustomButton
          text="Submit"
          onPress={() => navigation.navigate("Home")}
        />

        <CustomButton
          text="Back to Sign In"
          onPress={() => navigation.navigate("SignIn")}
          type="TRETIARY"
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#051C60",
    margin: 30,
  },
  terms: {
    color: "gray",
    marginVertical: 10,
  },
  link: {
    color: "#98e365",
  },
});

export default NewPasswordScreen;
