import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

const HistoryScreen = () => {
  const [mealHistory, setMealHistory] = useState([]);

  useEffect(() => {
    // Fetch meal history from AsyncStorage
    const fetchMealHistory = async () => {
      try {
        const storedHistory = await AsyncStorage.getItem("mealHistory");
        if (storedHistory) {
          setMealHistory(JSON.parse(storedHistory));
        }
      } catch (error) {
        console.error("Error fetching meal history:", error);
      }
    };

    fetchMealHistory();
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Meal History</Text>
      {mealHistory.length === 0 ? (
        <Text style={styles.emptyText}>No meals recorded yet.</Text>
      ) : (
        <FlatList
          data={mealHistory}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <View style={styles.historyItem}>
              <Text>{item.name}</Text>
              <Text>
                Completed at: {new Date(item.completedAt).toLocaleString()}
              </Text>
            </View>
          )}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 16,
  },
  emptyText: {
    fontSize: 18,
    fontStyle: "italic",
  },
  historyItem: {
    backgroundColor: "#f0f0f0",
    padding: 12,
    marginVertical: 8,
    borderRadius: 8,
  },
});

export default HistoryScreen;
