/* eslint-disable prettier/prettier */

import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Linking, Alert } from 'react-native';

const ContactUsScreen = () => {
  const handleEmailPress = () => {
    Linking.openURL('mailto:nomifyapp@gmail.com');
  };

  const handleCallPress = () => {
    Linking.openURL('tel:09297723652');
  };

  const handleMessagePress = () => {
    Linking.openURL('sms:09297723652');
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Contact Us</Text>
      <TouchableOpacity onPress={handleEmailPress}>
        <Text style={styles.text}>Email: nomifyapp@gmail.com</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={handleCallPress}>
        <Text style={styles.text}>Call: 09297723652</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={handleMessagePress}>
        <Text style={styles.text}>Message: 09297723652</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    backgroundColor: '#f5f5f5',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  text: {
    fontSize: 18,
    color: 'blue',
    textDecorationLine: 'underline',
    marginBottom: 10,
  },
});

export default ContactUsScreen;

