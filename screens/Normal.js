import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import normalData from "./NormalData";

const Normal = () => {
  const [selectedDay, setSelectedDay] = useState(1);
  const [completedHistory, setCompletedHistory] = useState([]);

  const [meals, setMeals] = useState({
    1: {
      breakfast:
        normalData.breakfast[
          Math.floor(Math.random() * normalData.breakfast.length)
        ],
      lunch:
        normalData.lunch[Math.floor(Math.random() * normalData.lunch.length)],
      dinner:
        normalData.dinner[Math.floor(Math.random() * normalData.dinner.length)],
    },
    2: {
      breakfast:
        normalData.breakfast[
          Math.floor(Math.random() * normalData.breakfast.length)
        ],
      lunch:
        normalData.lunch[Math.floor(Math.random() * normalData.lunch.length)],
      dinner:
        normalData.dinner[Math.floor(Math.random() * normalData.dinner.length)],
    },
    3: {
      breakfast:
        normalData.breakfast[
          Math.floor(Math.random() * normalData.breakfast.length)
        ],
      lunch:
        normalData.lunch[Math.floor(Math.random() * normalData.lunch.length)],
      dinner:
        normalData.dinner[Math.floor(Math.random() * normalData.dinner.length)],
    },
    4: {
      breakfast:
        normalData.breakfast[
          Math.floor(Math.random() * normalData.breakfast.length)
        ],
      lunch:
        normalData.lunch[Math.floor(Math.random() * normalData.lunch.length)],
      dinner:
        normalData.dinner[Math.floor(Math.random() * normalData.dinner.length)],
    },
    5: {
      breakfast:
        normalData.breakfast[
          Math.floor(Math.random() * normalData.breakfast.length)
        ],
      lunch:
        normalData.lunch[Math.floor(Math.random() * normalData.lunch.length)],
      dinner:
        normalData.dinner[Math.floor(Math.random() * normalData.dinner.length)],
    },
    6: {
      breakfast:
        normalData.breakfast[
          Math.floor(Math.random() * normalData.breakfast.length)
        ],
      lunch:
        normalData.lunch[Math.floor(Math.random() * normalData.lunch.length)],
      dinner:
        normalData.dinner[Math.floor(Math.random() * normalData.dinner.length)],
    },
    7: {
      breakfast:
        normalData.breakfast[
          Math.floor(Math.random() * normalData.breakfast.length)
        ],
      lunch:
        normalData.lunch[Math.floor(Math.random() * normalData.lunch.length)],
      dinner:
        normalData.dinner[Math.floor(Math.random() * normalData.dinner.length)],
    },
  });

  const navigation = useNavigation();

  const handleDayChange = (day) => {
    const today = new Date().getDay() || 7;

    if (day > today) {
      Alert.alert("Access Denied", "You cannot access future days.");
      return;
    }

    setSelectedDay(day);
    if (!meals[day]) {
      setMeals((prevMeals) => ({
        ...prevMeals,
        [day]: {
          breakfast:
            normalData.breakfast[
              Math.floor(Math.random() * normalData.breakfast.length)
            ],
          lunch:
            normalData.lunch[
              Math.floor(Math.random() * normalData.lunch.length)
            ],
          dinner:
            normalData.dinner[
              Math.floor(Math.random() * normalData.dinner.length)
            ],
        },
      }));
    }
  };

  return (
    <ScrollView style={styles.container}>
      <View style={styles.header}>
        <View style={styles.logoContainer}>
          <View style={styles.circle} />
          <Text style={styles.appName}>Nomify Normal</Text>
        </View>
        <View style={styles.tabsContainer}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <TouchableOpacity
              style={[
                styles.tabButton,
                selectedDay === 1 && styles.selectedTab,
              ]}
              onPress={() => handleDayChange(1)}
            >
              <View
                style={[
                  styles.circleButton,
                  selectedDay === 1 && styles.selectedCircle,
                ]}
              >
                <Text
                  style={[
                    styles.tabButtonText,
                    selectedDay === 1 && styles.selectedText,
                  ]}
                >
                  {selectedDay === 1 ? "Day 1" : "1"}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.tabButton,
                selectedDay === 2 && styles.selectedTab,
              ]}
              onPress={() => handleDayChange(2)}
            >
              <View
                style={[
                  styles.circleButton,
                  selectedDay === 2 && styles.selectedCircle,
                ]}
              >
                <Text
                  style={[
                    styles.tabButtonText,
                    selectedDay === 2 && styles.selectedText,
                  ]}
                >
                  {selectedDay === 2 ? "Day 2" : "2"}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.tabButton,
                selectedDay === 3 && styles.selectedTab,
              ]}
              onPress={() => handleDayChange(3)}
            >
              <View
                style={[
                  styles.circleButton,
                  selectedDay === 3 && styles.selectedCircle,
                ]}
              >
                <Text
                  style={[
                    styles.tabButtonText,
                    selectedDay === 3 && styles.selectedText,
                  ]}
                >
                  {selectedDay === 3 ? "Day 3" : "3"}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.tabButton,
                selectedDay === 4 && styles.selectedTab,
              ]}
              onPress={() => handleDayChange(4)}
            >
              <View
                style={[
                  styles.circleButton,
                  selectedDay === 4 && styles.selectedCircle,
                ]}
              >
                <Text
                  style={[
                    styles.tabButtonText,
                    selectedDay === 4 && styles.selectedText,
                  ]}
                >
                  {selectedDay === 4 ? "Day 4" : "4"}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.tabButton,
                selectedDay === 5 && styles.selectedTab,
              ]}
              onPress={() => handleDayChange(5)}
            >
              <View
                style={[
                  styles.circleButton,
                  selectedDay === 5 && styles.selectedCircle,
                ]}
              >
                <Text
                  style={[
                    styles.tabButtonText,
                    selectedDay === 5 && styles.selectedText,
                  ]}
                >
                  {selectedDay === 5 ? "Day 5" : "5"}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.tabButton,
                selectedDay === 6 && styles.selectedTab,
              ]}
              onPress={() => handleDayChange(6)}
            >
              <View
                style={[
                  styles.circleButton,
                  selectedDay === 6 && styles.selectedCircle,
                ]}
              >
                <Text
                  style={[
                    styles.tabButtonText,
                    selectedDay === 6 && styles.selectedText,
                  ]}
                >
                  {selectedDay === 6 ? "Day 6" : "6"}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.tabButton,
                selectedDay === 7 && styles.selectedTab,
              ]}
              onPress={() => handleDayChange(7)}
            >
              <View
                style={[
                  styles.circleButton,
                  selectedDay === 7 && styles.selectedCircle,
                ]}
              >
                <Text
                  style={[
                    styles.tabButtonText,
                    selectedDay === 7 && styles.selectedText,
                  ]}
                >
                  {selectedDay === 7 ? "Day 7" : "7"}
                </Text>
              </View>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </View>
      <View style={styles.mealContainer}>
        <Text style={styles.mealLabel}>Breakfast:</Text>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("NormalIngredients", {
              meal: meals[selectedDay].breakfast.name,
            })
          }
        >
          <View style={styles.mealItem}>
            <Image
              source={meals[selectedDay].breakfast.image}
              style={styles.roundedRectangle}
            />
            <View style={styles.mealNameContainer}>
              <Text style={styles.mealName}>
                {meals[selectedDay].breakfast.name}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        <Text style={styles.mealLabel}>Lunch:</Text>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("NormalIngredients", {
              meal: meals[selectedDay].lunch.name,
            })
          }
        >
          <View style={styles.mealItem}>
            <Image
              source={meals[selectedDay].lunch.image}
              style={styles.roundedRectangle}
            />
            <View style={styles.mealNameContainer}>
              <Text style={styles.mealName}>
                {meals[selectedDay].lunch.name}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        <Text style={styles.mealLabel}>Dinner:</Text>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("NormalIngredients", {
              meal: meals[selectedDay].dinner.name,
            })
          }
        >
          <View style={styles.mealItem}>
            <Image
              source={meals[selectedDay].dinner.image}
              style={styles.roundedRectangle}
            />
            <View style={styles.mealNameContainer}>
              <Text style={styles.mealName}>
                {meals[selectedDay].dinner.name}
              </Text>
            </View>
          </View>
        </TouchableOpacity>

        <View style={styles.historyContainer}>
          <TouchableOpacity
            style={styles.historyButton}
            onPress={() => {
              // Handle showing history
              console.log(completedHistory);
              // Implement navigation or modal to show history
            }}
          >
            <Text style={styles.historyButtonText}>Show History</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  header: {
    height: 150,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "left",
    marginLeft: 10,
    marginTop: 20,
  },
  logoContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginBottom: 40,
    marginTop: 40,
  },
  circle: {
    width: 30,
    height: 30,
    borderRadius: 20,
    backgroundColor: "#00FF00",
    marginRight: 10,
  },
  appName: {
    fontSize: 24,
    fontWeight: "bold",
    textAlign: "left",
  },
  tabsContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    paddingLeft: 10,
  },
  tabButton: {
    marginRight: 20,
  },
  circleButton: {
    borderRadius: 30,
    backgroundColor: "#000",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 16,
    paddingRight: 16,
  },
  tabButtonText: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#fff",
  },
  mealContainer: {
    flex: 1,
    padding: 20,
  },
  mealRow: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 20,
  },
  mealItem: {
    alignItems: "center",
  },
  roundedRectangle: {
    width: 310,
    height: 200,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: "#ccc",
    marginBottom: 10,
    marginTop: 20,
  },
  mealNameContainer: {
    backgroundColor: "#000",
    padding: 10,
    width: 310,
    height: 100,
    marginTop: -60,
    marginBottom: 30,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  mealName: {
    fontSize: 16,
    textAlign: "center",
    color: "#fff",
    fontWeight: "bold",
  },
  mealLabel: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10,
    marginTop: 40,
  },
  selectedTab: {
    backgroundColor: "#ccc",
    borderRadius: 10,
  },
  selectedCircle: {
    backgroundColor: "#ccc",
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 18,
    paddingRight: 18,
  },
  selectedText: {
    color: "#666",
  },
  historyContainer: {
    alignItems: "center",
    marginTop: 20,
  },
  historyButton: {
    backgroundColor: "#007AFF",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 8,
  },
  historyButtonText: {
    color: "#fff",
    fontSize: 16,
    fontWeight: "bold",
  },
});

export default Normal;
