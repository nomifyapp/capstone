import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
} from "react-native";
import { useRoute, useNavigation } from "@react-navigation/native";
import { Ionicons } from "@expo/vector-icons";
import overweightData from "./OverweightData";

const OverweightIngredients = () => {
  const route = useRoute();
  const navigation = useNavigation();
  const overMealName = route.params.meal;

  // Use Object.entries to find the correct meal type (breakfast, lunch, dinner)
  const [mealType, meals] = Object.entries(overweightData).find(
    ([key, meals]) =>
      meals.some(
        (meal) => meal.name.toLowerCase() === overMealName.toLowerCase()
      )
  );

  let overweightItem;
  if (mealType) {
    overweightItem = meals.find(
      (meal) => meal.name.toLowerCase() === overMealName.toLowerCase()
    );
  }

  if (!overweightItem) {
    // If meal item is not found, show an alert and navigate back
    Alert.alert("Meal Not Found", "The selected meal was not found.");
    navigation.goBack();
    return null;
  }

  return (
    <ScrollView style={styles.container}>
      <View style={styles.mealImageContainer}>
        <Image source={overweightItem.image} style={styles.mealImage} />
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.backButton}
        >
          <Ionicons name="arrow-back" size={24} color="white" />
        </TouchableOpacity>
      </View>
      <View style={styles.mealInfoContainer}>
        <Text style={styles.overMealName}>{overMealName}</Text>
      </View>
      <View style={styles.ingredientsContainer}>
        <Text style={styles.ingredientsLabel}>Ingredients:</Text>
        {overweightItem.ingredients.map((ingredient, index) => (
          <View key={index} style={styles.ingredientItem}>
            <Text style={styles.ingredientText}>• {ingredient}</Text>
          </View>
        ))}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mealImageContainer: {
    position: "relative",
  },
  mealImage: {
    width: "100%",
    height: 300,
    backgroundColor: "#ccc",
  },
  backButton: {
    position: "absolute",
    top: 30,
    left: 10,
    backgroundColor: "black",
    padding: 10,
    borderRadius: 25,
    marginTop: 20,
  },
  mealInfoContainer: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    marginTop: -20,
  },
  overMealName: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
    marginTop: 20,
  },
  ingredientsContainer: {
    backgroundColor: "white",
    borderRadius: 10,
    padding: 20,
    minHeight: 500, // Ensure ingredients list always has some minimum height
  },
  ingredientsLabel: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
  },
  ingredientItem: {
    marginBottom: 10,
    marginLeft: 30,
  },
  ingredientText: {
    fontSize: 15,
  },
});

export default OverweightIngredients;
