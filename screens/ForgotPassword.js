import { View, Text, Image, StyleSheet, ScrollView } from "react-native";
import React, { useState } from "react";
import Logo from "../../../assets/images/nomify-logo.png";
import CustomInput from "../components/CustomInput/CustomInput";
import CustomButton from "../components/CustomButton/CustomButton";
import SocialSignInButton from "../components/SocialSignInButton/SocialSignInButton";
import { useNavigation } from "@react-navigation/native";
import { firebase } from "../../../services/firebaseAuth";

const ForgotPasswordScreen = () => {
  const navigation = useNavigation();

  const [email, setEmail] = useState("");

  const changePassword = () => {
    const [email, setEmail] = useState([]);

    firebase
      .auth()
      .sendPasswordResetEmail(firebase.auth().currentUser.email)
      .then(() => {
        Alert.alert("Password reset email sent");
      })
      .catch((error) => {
        Alert.alert("Email not found", error);
      });
  };

  const onSendPressed = () => {
    console.warn("Register");
  };

  const onTermOfUse = () => {
    console.warn("Terms of Use");
  };

  const onPrivacyPolicy = () => {
    console.warn("Privacy Policy");
  };

  const onResendPress = () => {
    console.warn("Sign In");
  };

  const onSignInPress = () => {
    console.warn("Sign In");
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.root}>
        <Image source={Logo} />
        <Text style={styles.title}>Confirm your Email</Text>
        <CustomInput placeholder="Email" value={email} setValue={setEmail} />

        <CustomButton text="Send" onPress={changePassword} />

        <CustomButton
          text="Back to Sign In"
          onPress={() => navigation.navigate("SignIn")}
          type="TRETIARY"
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#051C60",
    margin: 30,
  },
  terms: {
    color: "gray",
    marginVertical: 10,
  },
  link: {
    color: "#98e365",
  },
});

export default ForgotPasswordScreen;
