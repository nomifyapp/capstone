/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, Alert } from "react-native";
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import CustomInput from "../components/CustomInput";
import CustomButton from "../components/CustomButton/CustomButton";

const HomeScreen = () => {
  const navigation = useNavigation();
  const [weight, setWeight] = useState("");
  const [height, setHeight] = useState("");
  const [age, setAge] = useState("");
  const [sex, setSex] = useState("");

  useEffect(() => {
    const checkUserBMI = async () => {
      try {
        const storedBMI = await AsyncStorage.getItem("userBMI");
        const storedScreen = await AsyncStorage.getItem("userScreen");
        if (storedBMI && storedScreen) {
          navigation.navigate(storedScreen);
        }
      } catch (error) {
        console.log("Error reading user data", error);
      }
    };

    checkUserBMI();
  }, [navigation]);

  const calculateBMI = async () => {
    if (
      !weight ||
      !height ||
      !age ||
      !sex ||
      isNaN(weight) ||
      isNaN(height) ||
      isNaN(age) ||
      !["male", "female", "Male", "Female"].includes(sex.toString())
    ) {
      Alert.alert(
        "Invalid Input",
        "Please enter valid numeric values for weight, height, and age. Sex should be either 'male' or 'female'."
      );
      return;
    }

    const weightInKg = parseFloat(weight);
    const heightInMeters = parseFloat(height) / 100;
    const bmi = weightInKg / (heightInMeters * heightInMeters);

    let screen = "";

    if (bmi > 0 && bmi < 18.5) {
      screen = "Underweight";
    } else if (bmi >= 18.5 && bmi < 24.9) {
      screen = "Normal";
    } else if (bmi >= 25 && bmi < 29.9) {
      screen = "Overweight";
    } else if (bmi >= 30) {
      screen = "Obese";
    } else {
      Alert.alert(
        "Error input! Enter another input",
        "Must not be negative or zero value"
      );
    }

    try {
      await AsyncStorage.setItem("userBMI", bmi.toString());
      await AsyncStorage.setItem("userScreen", screen);
      navigation.navigate(screen);
    } catch (error) {
      console.log("Error saving user data", error);
    }
  };

  const goToUserProfile = () => {
    navigation.navigate('UserProfile', { age, weight, height });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Welcome to NOMIFY!</Text>
      <Text style={styles.p}>Please Enter all Required fields</Text>
      <Text>
        Weight (kg): <Text style={styles.asterisk}>*</Text>
      </Text>
      <CustomInput
        style={styles.input}
        value={weight}
        setValue={setWeight}
        keyboardType="numeric"
      />
      <Text>
        Height (cm): <Text style={styles.asterisk}>*</Text>
      </Text>
      <CustomInput
        style={styles.input}
        value={height}
        setValue={setHeight}
        keyboardType="numeric"
      />
      <Text>
        Age: <Text style={styles.asterisk}>*</Text>
      </Text>
      <CustomInput
        style={styles.input}
        value={age}
        setValue={setAge}
        keyboardType="numeric"
      />
      <Text>
        Sex: <Text style={styles.asterisk}>*</Text>
      </Text>
      <CustomInput style={styles.input} value={sex} setValue={setSex} />
      <CustomButton text="Calculate BMI" onPress={calculateBMI} />
      <CustomButton text="View Profile" onPress={goToUserProfile} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    padding: 16,
  },
  input: {
    marginBottom: 12,
  },
  asterisk: {
    color: "red",
  },
  title: {
    position: "relative",
    bottom: 80,
    left: 35,
    fontSize: 24,
    fontWeight: "bold",
    color: "#051C60",
    margin: 30,
  },
  p: { position: "relative", bottom: 80, left: 50, fontSize: 18 },
});

export default HomeScreen;
