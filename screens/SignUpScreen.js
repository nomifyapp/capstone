import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Alert,
} from "react-native";
import React, { useState } from "react";
import Logo from "../../../assets/images/nomify-logo.png";
import CustomInput from "../components/CustomInput/CustomInput";
import CustomButton from "../components/CustomButton/CustomButton";
import SocialSignInButton from "../components/SocialSignInButton/SocialSignInButton";
import { useNavigation } from "@react-navigation/native";
import { Ionicons } from "@expo/vector-icons";
import auth from "../../../services/firebaseAuth";
import { createUserWithEmailAndPassword } from "firebase/auth";

const SignUpScreen = () => {
  const navigation = useNavigation();

  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordVisible, setPasswordVisible] = useState(false);
  const [passwordRepeat, setPasswordRepeat] = useState("");
  const [passwordRepeatVisible, setPasswordRepeatVisible] = useState(false);

  const onRegisterPressed = () => {
    if (password !== passwordRepeat) {
      console.error("Passwords do not match");
      return;
    }

    createUserWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        console.log(user);
        Alert.alert("Sucess!", "You may now login");
        // Navigate to the confirm screen or any other screen after successful registration
        navigation.navigate("SignIn");
      })
      .catch((error) => {
        Alert.alert("Regristration Error! Email already in use", error.message);
      });
  };

  const onTermOfUse = () => {
    console.warn("Terms of Use");
  };

  const onPrivacyPolicy = () => {
    console.warn("Privacy Policy");
  };

  const onSignInPressed = () => {
    navigation.navigate("SignIn");
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.root}>
        <Image source={Logo} />
        <Text style={styles.title}>Create an Account</Text>

        <CustomInput placeholder="Email" value={email} setValue={setEmail} />
        <CustomInput
          placeholder="Password"
          value={password}
          setValue={setPassword}
          secureTextEntry={!passwordVisible}
        />
        <TouchableOpacity
          onPress={() => setPasswordVisible(!passwordVisible)}
          style={styles.passicon}
        >
          <Ionicons name={passwordVisible ? "eye" : "eye-off"} size={24} />
        </TouchableOpacity>
        <CustomInput
          placeholder="Repeat Password"
          value={passwordRepeat}
          setValue={setPasswordRepeat}
          secureTextEntry={!passwordRepeatVisible}
        />
        <TouchableOpacity
          onPress={() => setPasswordRepeatVisible(!passwordRepeatVisible)}
          style={styles.passicon2}
        >
          <Ionicons
            name={passwordRepeatVisible ? "eye" : "eye-off"}
            size={24}
          />
        </TouchableOpacity>
        <CustomButton text="Register" onPress={onRegisterPressed} />
        <Text style={styles.terms}>
          By registering, you confirm that you accept our
          <Text style={styles.link} onPress={onTermOfUse}>
            {" "}
            Term of Use{" "}
          </Text>
          and
          <Text style={styles.link} onPress={onPrivacyPolicy}>
            {" "}
            Privacy Policy{" "}
          </Text>
        </Text>
        <CustomButton
          text="Have an Account? Sign in"
          onPress={onSignInPressed}
          type="TRETIARY"
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#051C60",
    margin: 30,
  },
  terms: {
    color: "gray",
    marginVertical: 10,
  },
  link: {
    color: "#98e365",
  },
  passicon: {
    position: "relative",
    bottom: 43,
    left: 150,
  },
  passicon2: {
    position: "relative",
    bottom: 43,
    left: 148,
  },
});

export default SignUpScreen;
