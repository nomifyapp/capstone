/* eslint-disable prettier/prettier */
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const MetricsScreen = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Metrics</Text>
      <Text style={styles.text}>Progress on food intake will be tracked here.</Text>
      {/* e2 ung checklist na sabi ni maam */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    backgroundColor: '#f5f5f5',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  text: {
    fontSize: 18,
    marginBottom: 10,
  },
});

export default MetricsScreen;
