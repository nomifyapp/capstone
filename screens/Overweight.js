import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import overweightData from "./OverweightData";

const Overweight = () => {
  const navigation = useNavigation();
  const [selectedDay, setSelectedDay] = useState(new Date().getDay() || 7); // Sunday is 0, set to 7 for consistency with your week

  const [meals, setMeals] = useState({
    1: getRandomMeals(),
    2: getRandomMeals(),
    3: getRandomMeals(),
    4: getRandomMeals(),
    5: getRandomMeals(),
    6: getRandomMeals(),
    7: getRandomMeals(),
  });

  useEffect(() => {
    handleDayChange(selectedDay);
  }, [selectedDay]);

  function getRandomMeals() {
    return {
      breakfast:
        overweightData.breakfast[
          Math.floor(Math.random() * overweightData.breakfast.length)
        ],
      lunch:
        overweightData.lunch[
          Math.floor(Math.random() * overweightData.lunch.length)
        ],
      dinner:
        overweightData.dinner[
          Math.floor(Math.random() * overweightData.dinner.length)
        ],
    };
  }

  const handleDayChange = (day) => {
    const today = new Date().getDay() || 7; // Sunday is 0, so || 7 for Sunday

    // Allow selection of today or previous days
    if (day > today) {
      Alert.alert("Access Denied", "You cannot access future days.");
      return;
    }

    setSelectedDay(day);

    if (!meals[day]) {
      setMeals((prevMeals) => ({
        ...prevMeals,
        [day]: getRandomMeals(),
      }));
    }
  };

  return (
    <ScrollView style={styles.container}>
      <View style={styles.header}>
        <View style={styles.logoContainer}>
          <View style={styles.circle} />
          <Text style={styles.appName}>Nomify Overweight</Text>
        </View>
        <View style={styles.tabsContainer}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {Array.from({ length: 7 }, (_, i) => i + 1).map((day) => (
              <TouchableOpacity
                key={day}
                style={[
                  styles.tabButton,
                  selectedDay === day && styles.selectedTab,
                ]}
                onPress={() => handleDayChange(day)}
              >
                <View
                  style={[
                    styles.circleButton,
                    selectedDay === day && styles.selectedCircle,
                  ]}
                >
                  <Text
                    style={[
                      styles.tabButtonText,
                      selectedDay === day && styles.selectedText,
                    ]}
                  >
                    {selectedDay === day ? `Day ${day}` : day.toString()}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      </View>
      <View style={styles.mealContainer}>
        <Text style={styles.mealLabel}>Breakfast:</Text>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("OverweightIngredients", {
              meal: meals[selectedDay].breakfast.name,
            })
          }
        >
          <View style={styles.mealItem}>
            <Image
              source={meals[selectedDay].breakfast.image}
              style={styles.roundedRectangle}
            />
            <View style={styles.mealNameContainer}>
              <Text style={styles.mealName}>
                {meals[selectedDay].breakfast.name}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        <Text style={styles.mealLabel}>Lunch:</Text>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("OverweightIngredients", {
              meal: meals[selectedDay].lunch.name,
            })
          }
        >
          <View style={styles.mealItem}>
            <Image
              source={meals[selectedDay].lunch.image}
              style={styles.roundedRectangle}
            />
            <View style={styles.mealNameContainer}>
              <Text style={styles.mealName}>
                {meals[selectedDay].lunch.name}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        <Text style={styles.mealLabel}>Dinner:</Text>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("OverweightIngredients", {
              meal: meals[selectedDay].dinner.name,
            })
          }
        >
          <View style={styles.mealItem}>
            <Image
              source={meals[selectedDay].dinner.image}
              style={styles.roundedRectangle}
            />
            <View style={styles.mealNameContainer}>
              <Text style={styles.mealName}>
                {meals[selectedDay].dinner.name}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  header: {
    height: 150,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "left",
    marginLeft: 10,
    marginTop: 20,
  },
  logoContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginBottom: 40,
    marginTop: 40,
  },
  circle: {
    width: 30,
    height: 30,
    borderRadius: 20,
    backgroundColor: "#00FF00",
    marginRight: 10,
  },
  appName: {
    fontSize: 24,
    fontWeight: "bold",
    textAlign: "left",
  },
  tabsContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    paddingLeft: 10,
  },
  tabButton: {
    marginRight: 20,
  },
  circleButton: {
    borderRadius: 30,
    backgroundColor: "#000",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 16,
    paddingRight: 16,
  },
  tabButtonText: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#fff",
  },
  mealContainer: {
    flex: 1,
    padding: 20,
  },
  mealRow: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 20,
  },
  mealItem: {
    alignItems: "center",
  },
  roundedRectangle: {
    width: 310,
    height: 200,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: "#ccc",
    marginBottom: 10,
    marginTop: 20,
  },
  mealNameContainer: {
    backgroundColor: "#000",
    padding: 10,
    width: 310,
    height: 100,
    marginTop: -60,
    marginBottom: 30,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  mealName: {
    fontSize: 16,
    textAlign: "center",
    color: "#fff",
    fontWeight: "bold",
  },
  mealLabel: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10,
    marginTop: 40,
  },
  selectedTab: {
    backgroundColor: "#ccc",
    borderRadius: 10,
  },
  selectedCircle: {
    backgroundColor: "#ccc",
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 18,
    paddingRight: 18,
  },
  selectedText: {
    color: "#666",
  },
});

export default Overweight;
